/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.tictactoeexam;

import com.progmatic.tictactoeexam.enums.PlayerType;
import com.progmatic.tictactoeexam.exceptions.CellException;
import com.progmatic.tictactoeexam.interfaces.Board;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author lacikiska
 */
public class BoardSimulator implements Board {

    ArrayList<Cell> firstRow;
    ArrayList<Cell> secondRow;
    ArrayList<Cell> thirdRow;
    

    public BoardSimulator() {

        firstRow = new ArrayList<>();
        secondRow = new ArrayList<>();
        thirdRow = new ArrayList<>();

        for (int i = 0; i < 3; i++) {

            Cell fill = new Cell(0, i);
            firstRow.add(fill);

            Cell fillSec = new Cell(1, i);
            secondRow.add(fillSec);

            Cell fillThird = new Cell(2, i);
            thirdRow.add(fillThird);
        }
    }

    @Override
    public PlayerType getCell(int rowIdx, int colIdx) throws CellException {

        PlayerType p = null;
        if (colIdx > 2) {
            throw new CellException(rowIdx,colIdx," Invalid Index");
        }
        switch (rowIdx) {
            case 0:
                p = this.firstRow.get(colIdx).getCellsPlayer();
                break;
            case 1:
                p = this.secondRow.get(colIdx).getCellsPlayer();
                break;
            case 2:
                p = this.thirdRow.get(colIdx).getCellsPlayer();
                break;
            default:
                throw new CellException(rowIdx,colIdx," Invalid Index");
        }
        return p;
    }

    @Override
    public void put(Cell cell) throws CellException {

        if (this.getCell(cell.getRow(), cell.getCol()) == PlayerType.EMPTY) {

            if (cell.getCol() > 2 && cell.getRow() > 2) {
                throw new CellException(cell.getRow(),cell.getCol()," Invalid Index");
            } else {

                switch (cell.getRow()) {
                    case 0:
                        this.firstRow.set(cell.getCol(), cell);
                        break;
                    case 1:
                        this.secondRow.set(cell.getCol(), cell);
                        break;
                    case 2:
                        this.thirdRow.set(cell.getCol(), cell);
                        break;

                }

            }
        } else {
            throw new CellException(cell.getRow(),cell.getCol(),"Invalid Index");
        }

    }

    @Override
    public boolean hasWon(PlayerType p) {
        boolean hasWon = false;
        //Horizontal
        if (firstRow.get(0).getCellsPlayer() == p && firstRow.get(1).getCellsPlayer() == p
                && firstRow.get(2).getCellsPlayer() == p) {
            return true;
        }

        if (secondRow.get(0).getCellsPlayer() == p && secondRow.get(1).getCellsPlayer() == p
                && secondRow.get(2).getCellsPlayer() == p) {
            return true;
        }
        if (thirdRow.get(0).getCellsPlayer() == p && thirdRow.get(1).getCellsPlayer() == p
                && thirdRow.get(2).getCellsPlayer() == p) {
            return true;
        }

        //Vertical
        int counterV = 0;
        for (int i = 0; i < 3; i++) {
            counterV = 0;
            if (firstRow.get(i).getCellsPlayer() == p) {
                counterV = counterV + 1;

                if (secondRow.get(i).getCellsPlayer() == p) {
                    counterV = counterV + 1;

                    if (thirdRow.get(i).getCellsPlayer() == p) {
                        counterV = counterV + 1;
                    }
                    if (counterV == 3) {
                        return true;
                    }
                }
            }
        }

        //Diagonal
        if (firstRow.get(0).getCellsPlayer() == p && secondRow.get(1).getCellsPlayer() == p
                && thirdRow.get(2).getCellsPlayer() == p) {
            return true;
        }
        if (firstRow.get(2).getCellsPlayer() == p && secondRow.get(1).getCellsPlayer() == p
                && thirdRow.get(0).getCellsPlayer() == p) {
            return true;
        }

        return hasWon;
    }

    @Override
    public List<Cell> emptyCells() {

        List<Cell> emptyCells = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            if (firstRow.get(i).getCellsPlayer() == PlayerType.EMPTY) {
                emptyCells.add(firstRow.get(i));
            }
            if (secondRow.get(i).getCellsPlayer() == PlayerType.EMPTY) {
                emptyCells.add(secondRow.get(i));
            }
            if (thirdRow.get(i).getCellsPlayer() == PlayerType.EMPTY) {
                emptyCells.add(thirdRow.get(i));
            }
        }
        return emptyCells;
    }

    public static void main(String[] args) throws CellException {

        BoardSimulator bs = new BoardSimulator();

//        bs.getCell(3, 0);
        
        Cell n=new Cell(1,1,PlayerType.X);
        Cell n2=new Cell(1,1,PlayerType.O);
        bs.put(n2);
        bs.put(n);

    }
}
