/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Players;

import com.progmatic.tictactoeexam.Cell;
import com.progmatic.tictactoeexam.enums.PlayerType;
import com.progmatic.tictactoeexam.exceptions.CellException;
import com.progmatic.tictactoeexam.interfaces.Board;
import com.progmatic.tictactoeexam.interfaces.Player;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author lacikiska
 */
public class VictoryAwarePlayer implements Player {

    @Override
    public Cell nextMove(Board b) {
        Cell newCell = null;
        //horizontal
        try {
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 3; j++) {
                    if (b.getCell(i, j)==PlayerType.EMPTY) {
                       newCell= new Cell(i,j,this.getMyType());
                    }
                }
            }
        } catch (CellException ex) {
            Logger.getLogger(VictoryAwarePlayer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return newCell;
    }

    @Override
    public PlayerType getMyType() {
        return PlayerType.X;
    }

}
