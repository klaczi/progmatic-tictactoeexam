/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Players;

import com.progmatic.tictactoeexam.Cell;
import com.progmatic.tictactoeexam.enums.PlayerType;
import com.progmatic.tictactoeexam.exceptions.CellException;
import com.progmatic.tictactoeexam.interfaces.Board;
import com.progmatic.tictactoeexam.interfaces.Player;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author lacikiska
 */
public class SimplePlayer implements Player {

    Board b;

    @Override
    public Cell nextMove(Board b) {
        int counter = 0;
        Cell newCell = null;

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                try {

                    if (b.getCell(i, j) == PlayerType.EMPTY) {
                        counter = counter + 1;
                        newCell = new Cell(i, j, this.getMyType());
                        return newCell;
                    }
                } catch (CellException ex) {
                }
            }
        }

        if (counter == 0) {
            return null;
        }
        return newCell;
    }

    @Override
    public PlayerType getMyType() {
        return PlayerType.O;
    }

}
